package com.rentit.inventory.rest.dto;

import lombok.Data;
import org.springframework.hateoas.core.Relation;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
@Relation(value="plant", collectionRelation="plants")
public class PlantInventoryEntryDTO {
    Long _id;
    String name;
    String description;
    @Column(precision = 8, scale = 2)
    BigDecimal price;
}
